##
## Makefile for Makefile in /home/bengle_b/rendu/psu_2014_malloc
## 
## Made by Bengler Bastien
## Login   <bengle_b@epitech.net>
## 
## Started on  Fri Jan 30 11:25:24 2015 Bengler Bastien
## Last update Sun Feb 15 18:04:51 2015 Bengler Bastien
##

NAME		= libmy_malloc_$(HOSTTYPE).so

LIB_LINK	= libmy_malloc.so

SRC		= files/malloc.c \
		  files/realloc.c \
		  files/free.c \
		  files/calloc.c \
		  files/show_alloc_mem.c

OBJ		= $(SRC:.c=.o)

CC		= gcc

RM		= rm -f

CFLAGS		= -fPIC -Wall -Wextra -Werror

LINK		= ln -s

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -shared -o $(NAME) $(OBJ)
	$(LINK) $(NAME) $(LIB_LINK)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)
	$(RM) $(LIB_LINK)

re: fclean all

.PHONY: clean fclean re all
