/*
** malloc.h for Malloc in /home/bengle_b/rendu/psu_2014_malloc/files
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Jan 30 11:25:35 2015 Bengler Bastien
** Last update Sun Feb 15 18:02:41 2015 Bengler Bastien
*/

#ifndef		MALLOC_H_
# define	MALLOC_H_

#include <stdlib.h>

typedef struct	s_mem
{
  size_t	size;
  unsigned int	isfree;
  struct s_mem	*next;
}		t_mem;

t_mem		*begin;
t_mem		*last_insert;

t_mem		*first_malloc(size_t taille);
t_mem		*malloc_at_the_end(size_t taille);
t_mem		*malloc_between(size_t taille, t_mem *tmp);
t_mem		*where_to_malloc(size_t taille);
void		*malloc(size_t taille);
void		*realloc(void *ptr, size_t taille);
void		*calloc(size_t nmemb, size_t size);
t_mem		*whereis_before_mem(t_mem *mem);
int		pointer_is_valid(void *ptr);
void		free(void *ptr);
void		show_alloc_mem();

#endif		/* !MALLOC_H_ */
