/*
** free.c for free in /home/bengle_b/rendu/psu_2014_malloc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Feb 13 15:40:12 2015 Bengler Bastien
** Last update Sun Feb 15 17:56:17 2015 Bengler Bastien
*/

#include <unistd.h>
#include "malloc.h"

int		pointer_is_valid(void *ptr)
{
  t_mem		*tmp;
  void		*a;
  t_mem		*mem;
  
  a = ptr;
  a = a - sizeof(t_mem);
  mem = a;
  tmp = begin;
  if (mem >= begin && mem <= last_insert)
    {
      while (tmp->next != NULL)
	{
	  if (mem == tmp)
	    return (1);
	  tmp = tmp->next;
	}
      if (mem == tmp)
	return (1);
    }
  return (0);
}

void		free(void *ptr)
{
  t_mem		*mem;
  void		*a;
  void		*my_sbrk;

  if ((my_sbrk = sbrk(0)) != (void*) -1)
    {
      if (ptr != NULL && begin != NULL && ptr < my_sbrk && ptr > (void*)begin)
	{
	  a = ptr;
	  a = a - sizeof(t_mem);
	  mem = a;
	  mem->isfree = 1;
	}
    }
}

t_mem		*whereis_before_mem(t_mem *mem)
{
  t_mem		*tmp;

  tmp = begin;
  while (tmp->next != mem && tmp->next != NULL)
    tmp = tmp->next;
 return (tmp);
}
