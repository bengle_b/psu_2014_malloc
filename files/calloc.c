/*
** calloc.c for calloc in /home/bengle_b/rendu/psu_2014_malloc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Feb 13 15:41:31 2015 Bengler Bastien
** Last update Fri Feb 13 15:45:25 2015 Bengler Bastien
*/

#include <string.h>
#include "malloc.h"

void		*calloc(size_t nmemb, size_t size)
{
  t_mem		*mem;

  if ((int)nmemb <= 0 || (int)size <= 0)
    return (NULL);
  else
    {
      if ((mem = malloc(nmemb * size)) == NULL)
	return (NULL);
      memset(mem, 0, (nmemb * size));
      return (mem);
    }
}
