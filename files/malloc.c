/*
** malloc.c for malloc in /home/bengle_b/rendu/psu_2014_malloc/files
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Mon Feb  9 15:06:54 2015 Bengler Bastien
** Last update Fri Feb 13 15:52:59 2015 Bengler Bastien
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "malloc.h"

t_mem		*first_malloc(size_t taille)
{
  t_mem		*mem;

  if ((mem = sbrk(taille + sizeof(t_mem))) == (void*) -1)
    return (NULL);
  mem->size = taille + sizeof(t_mem);
  mem->isfree = 0;
  mem->next = NULL;
  begin = mem;
  last_insert = mem;
  return (mem);
}

t_mem		*malloc_at_the_end(size_t taille)
{
  t_mem		*mem;

  if ((mem = sbrk(taille + sizeof(t_mem))) == (void*) -1)
    return (NULL);
  mem->size = taille + sizeof(t_mem);
  mem->isfree = 0;
  mem->next = NULL;
  last_insert->next = mem;
  last_insert = mem;
  return (mem);
}

t_mem		*malloc_between(size_t taille, t_mem *tmp)
{
  t_mem		*mem;
  t_mem		*split;
  t_mem		*a;
  void		*ptr;

  if (tmp->size > taille + sizeof(t_mem) + sizeof(t_mem))
    {
      a = tmp->next;
      mem = tmp;
      ptr = tmp;
      ptr += taille + sizeof(t_mem);
      mem->next = (t_mem*)ptr;
      split = mem->next;
      split->size = tmp->size - (taille + sizeof(t_mem));
      mem->size = taille + sizeof(t_mem);
      mem->isfree = 0;
      split->next = a;
      split->isfree = 1;
    }
  else
    {
      tmp->isfree = 0;
      mem = tmp;
    }
  return (mem);
}

t_mem		*where_to_malloc(size_t taille)
{
  t_mem		*mem;
  t_mem		*tmp;

  mem = NULL;
  tmp = begin;
  while (tmp->next != NULL)
    {
      if (tmp->isfree == 1 && tmp->size >= taille + sizeof(t_mem))
	mem = malloc_between(taille, tmp);
      if (mem != NULL)
	return (mem);
      tmp = tmp->next;
    }
  if (tmp->next == NULL && tmp->isfree == 1 && tmp->size >= taille + sizeof(t_mem))
    mem = malloc_between(taille, tmp);
  else if (tmp->next == NULL)
    mem = malloc_at_the_end(taille);
  return (mem);
}

void		*malloc(size_t taille)
{
  t_mem		*mem;
  void		*ptr;

  if ((int)taille <= 0)
    return (NULL);
  if ((begin) == NULL)
    mem = first_malloc(taille);
  else
    mem = where_to_malloc(taille);
  if (mem == NULL)
    return (NULL);
  ptr = mem;
  return (ptr + sizeof(t_mem));
}
