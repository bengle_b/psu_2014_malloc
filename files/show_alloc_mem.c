/*
** show_alloc_mem.c for show_alloc_mem.c in /home/bengle_b/rendu/psu_2014_malloc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Feb 13 15:41:53 2015 Bengler Bastien
** Last update Sun Feb 15 18:10:23 2015 Bengler Bastien
*/

#include <stdio.h>
#include <unistd.h>
#include "malloc.h"

void		show_alloc_mem()
{
  t_mem		*mem;
  t_mem		*tmp;
  void		*my_sbrk;

  if (begin != NULL && (my_sbrk = sbrk(0)) != (void*) -1)
    {
      mem = begin;
      tmp = begin;
      while (tmp->next)
	tmp = tmp->next;
      printf("break : %p\n", my_sbrk);
      while (mem->next)
	{
	  if (!mem->isfree)
	    printf("%p - %p : %d octets\n", mem, mem->next, (int)mem->size);
	  mem = mem->next;
	}
      if (!mem->isfree)
	printf("%p - %p : %d octets\n", mem, sbrk(0), (int)mem->size);
    }
}

