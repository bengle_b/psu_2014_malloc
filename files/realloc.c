/*
** realloc.c for realloc in /home/bengle_b/rendu/psu_2014_malloc
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Fri Feb 13 15:41:09 2015 Bengler Bastien
** Last update Sun Feb 15 18:03:40 2015 Bengler Bastien
*/

#include <unistd.h>
#include <string.h>
#include "malloc.h"

void		*realloc(void *ptr, size_t taille)
{
  t_mem		*mem;
  void		*new;
  void		*my_sbrk;

  new =  NULL;
  if ((int)taille <= 0)
    {
      free(ptr);
      return (NULL);
    }
  new = malloc(taille);
  if ((my_sbrk = sbrk(0)) == (void*) -1)
    return (new);
  if (ptr != NULL && begin != NULL && ptr < my_sbrk && ptr > (void*)begin &&
      (pointer_is_valid(ptr)) == 1)
    {
      mem = ptr - sizeof(t_mem);
      if (mem->size > taille)
	mem->size = taille;
      memcpy(new, ptr, taille);
      free(ptr);
    }
  return (new);
}
